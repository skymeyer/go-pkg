package discovery

import (
	"context"
	"fmt"
	"math/rand"
	"net/url"
	"time"
)

// List of predefined endpoint kinds.
const (
	KIND_TCP  = "tcp"
	KIND_UDP  = "udp"
	KIND_GRPC = "grpc"
	KIND_REST = "rest"
)

var (
	random = rand.New(rand.NewSource(time.Now().Unix()))
)

type Resolver interface {
	// FindAll returns all available endpoints for a given service kind/name ordered
	// by priority. If no endpoints are found, an error is returned.
	FindAll(ctx context.Context, kind, name string) ([]*Endpoint, error)

	// Find returns the most preferred endpoint for a given service kind/name. If no
	// endpoint is available, an error is returned.
	Find(ctx context.Context, kind, name string) (*Endpoint, error)
}

// Endpoint represent a service endpoint.
type Endpoint struct {
	// Kind of service (i.e. http, grpc, tcp, ...).
	Kind string

	// The Name of the service.
	Name string

	// The Hostname where the service is hosted.
	Hostname string

	// The Port on which the service can be reached.
	Port string
}

// URL returns the URL representation of the endpoint.
func (e *Endpoint) URL() *url.URL {
	u := &url.URL{
		Scheme: "http",
		Host:   e.Socket(),
	}
	if e.Port == "443" {
		u.Scheme = "https"
	}
	return u
}

// Socket returns the socket representation of the endpoint.
func (e *Endpoint) Socket() string {
	return fmt.Sprintf("%s:%s", e.Hostname, e.Port)
}

// URLToEndpoint is a convenience function to create an endpoint object
// for a given kind, name and URL.
func URLToEndpoint(kind, name, in string) (*Endpoint, error) {
	parsed, err := url.Parse(in)
	if err != nil {
		return nil, err
	}

	var port = parsed.Port()
	if port == "" {
		switch parsed.Scheme {
		case "http":
			port = "80"
		case "https":
			port = "443"
		default:
			return nil, fmt.Errorf("unknown scheme %q", parsed.Scheme)
		}
	}

	return &Endpoint{
		Kind:     kind,
		Name:     name,
		Hostname: parsed.Hostname(),
		Port:     port,
	}, nil
}
