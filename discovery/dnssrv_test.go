package discovery

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestDnsSrvResolver(t *testing.T) {
	t.Parallel()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	r := NewDnsSrvResolver("e2e.skymeyer.dev")
	eps, err := r.FindAll(ctx, "tcp", "test")

	assert.NoError(t, err)
	assert.Equal(t, []*Endpoint{
		&Endpoint{Kind: "tcp", Name: "test", Hostname: "ep1.e2e.skymeyer.dev", Port: "443"},
		&Endpoint{Kind: "tcp", Name: "test", Hostname: "ep2.e2e.skymeyer.dev", Port: "443"},
		&Endpoint{Kind: "tcp", Name: "test", Hostname: "ep3.e2e.skymeyer.dev", Port: "443"},
	}, eps)
}
