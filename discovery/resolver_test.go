package discovery

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEndpoint(t *testing.T) {
	for _, d := range []struct {
		ep  Endpoint
		url string
	}{
		{
			ep:  Endpoint{Hostname: "foo.com", Port: "443"},
			url: "https://foo.com:443",
		},
		{
			ep:  Endpoint{Hostname: "foo.org", Port: "80"},
			url: "http://foo.org:80",
		},
		{
			ep:  Endpoint{Hostname: "foo.biz", Port: "8080"},
			url: "http://foo.biz:8080",
		},
	} {
		assert.Equal(t, d.url, d.ep.URL().String())
	}
}

func TestURLToEndpoint(t *testing.T) {
	for _, d := range []struct {
		url string
		ep  *Endpoint
	}{
		{
			url: "https://foo.com",
			ep:  &Endpoint{Hostname: "foo.com", Port: "443"},
		},
		{
			url: "https://foo.com:443",
			ep:  &Endpoint{Hostname: "foo.com", Port: "443"},
		},
		{
			url: "https://foo.com:123",
			ep:  &Endpoint{Hostname: "foo.com", Port: "123"},
		},
		{
			url: "http://foo.com",
			ep:  &Endpoint{Hostname: "foo.com", Port: "80"},
		},
		{
			url: "http://foo.com:80",
			ep:  &Endpoint{Hostname: "foo.com", Port: "80"},
		},
		{
			url: "http://foo.com:123",
			ep:  &Endpoint{Hostname: "foo.com", Port: "123"},
		},
	} {
		// Fixture on kind and name
		d.ep.Kind = "foo"
		d.ep.Name = "bar"

		ep, err := URLToEndpoint("foo", "bar", d.url)
		assert.NoError(t, err)
		assert.Equal(t, d.ep, ep)
	}
}
