package discovery

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	staticResolver = &StaticResolver{
		Services: []*Endpoint{
			&Endpoint{Kind: "foo", Name: "bar", Hostname: "example.com"},
			&Endpoint{Kind: "foo", Name: "baz", Hostname: "example.org"},
		},
	}
)

func TestStaticResolver(t *testing.T) {
	for _, d := range []struct {
		kind, name string
		eps        Endpoint
	}{
		{
			"foo", "bar",
			Endpoint{Kind: "foo", Name: "bar", Hostname: "example.com"},
		},
		{
			"foo", "baz",
			Endpoint{Kind: "foo", Name: "baz", Hostname: "example.org"},
		},
	} {
		eps, err := staticResolver.Find(context.Background(), d.kind, d.name)
		assert.NoError(t, err)
		assert.Equal(t, &d.eps, eps)
	}
}

func TestStaticResolverAddByURL(t *testing.T) {
	r := &StaticResolver{}
	r.AddByURL("foo", "bar", "https://demo.com")

	ep, err := r.Find(context.Background(), "foo", "bar")
	assert.NoError(t, err)

	assert.Equal(t, "https://demo.com:443", ep.URL().String())
	assert.Equal(t, "demo.com:443", ep.Socket())
}
