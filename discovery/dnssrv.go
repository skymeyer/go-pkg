package discovery

import (
	"context"
	"fmt"
	"net"
)

var (
	ErrNoSrvRecordsFound = fmt.Errorf("no srv records found")
)

// NewDnsSrvResolver creats a DNS SRV record Resolver for a given domain.
func NewDnsSrvResolver(domain string, opts ...DnsSrvResolverOption) Resolver {
	var r = &DnsSrvResolver{
		domain:   domain,
		resolver: net.DefaultResolver,
	}
	for _, opt := range opts {
		opt(r)
	}
	return r
}

// DnsSrvResolver is DNS SRV record based Resolver.
type DnsSrvResolver struct {
	domain   string
	resolver *net.Resolver
}

// FindAll implements Resolver.
func (r *DnsSrvResolver) FindAll(ctx context.Context, kind, name string) ([]*Endpoint, error) {

	// The returned srvs are ordered by priority and weighted.
	_, srvs, err := r.resolver.LookupSRV(ctx, name, kind, r.domain)
	if err != nil {
		return nil, err
	}

	if len(srvs) < 1 {
		return nil, ErrNoSrvRecordsFound
	}

	var eps []*Endpoint
	for _, srv := range srvs {
		eps = append(eps, &Endpoint{
			Kind:     kind,
			Name:     name,
			Hostname: srv.Target[:len(srv.Target)-1], // strip trailing dot from FQDN
			Port:     fmt.Sprintf("%d", srv.Port),
		})
	}
	return eps, nil
}

// Find implements Resolver.
func (r *DnsSrvResolver) Find(ctx context.Context, kind, name string) (*Endpoint, error) {
	eps, err := r.FindAll(ctx, kind, name)
	if err != nil {
		return nil, err
	}
	return eps[0], nil
}

type DnsSrvResolverOption func(*DnsSrvResolver)

// WithResolver overrides the default net.Resolver.
func WithResolver(r *net.Resolver) DnsSrvResolverOption {
	return func(d *DnsSrvResolver) {
		d.resolver = r
	}
}
