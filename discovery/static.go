package discovery

import (
	"context"
	"fmt"
)

var (
	ErrServiceNotFound = fmt.Errorf("service not found")
)

// StaticResolver uses a predfined list of Services.
type StaticResolver struct {
	Services []*Endpoint
}

// FindAll implements Resolver.
func (r *StaticResolver) FindAll(ctx context.Context, kind, name string) ([]*Endpoint, error) {
	var eps []*Endpoint
	for _, ep := range r.Services {
		if ep.Kind == kind && ep.Name == name {
			eps = append(eps, ep)
		}
	}

	if len(eps) == 0 {
		return nil, ErrServiceNotFound
	}

	random.Shuffle(len(eps), func(i, j int) {
		eps[i], eps[j] = eps[j], eps[i]
	})

	return eps, nil
}

// Find implements Resolver.
func (r *StaticResolver) Find(ctx context.Context, kind, name string) (*Endpoint, error) {
	eps, err := r.FindAll(ctx, kind, name)
	if err != nil {
		return nil, err
	}
	return eps[0], nil
}

// AddByURL adds a new service by URL to the static list.
func (r *StaticResolver) AddByURL(kind, name, in string) error {
	ep, err := URLToEndpoint(kind, name, in)
	if err != nil {
		return err
	}
	r.Services = append(r.Services, ep)
	return nil
}
