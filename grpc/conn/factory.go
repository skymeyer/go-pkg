package conn

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/keepalive"

	"go.skymeyer.dev/pkg/discovery"
)

type Factory struct {
	resolver   discovery.Resolver
	rootCaPool *x509.CertPool
	dialOpts   []grpc.DialOption
	insecure   bool
}

func NewFactory(r discovery.Resolver, opts ...FactoryOption) *Factory {
	var f = &Factory{resolver: r}
	for _, opt := range opts {
		opt(f)
	}
	return f
}

func (f *Factory) New(ctx context.Context, service string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	svc, err := f.resolver.Find(ctx, discovery.KIND_GRPC, service)
	if err != nil {
		return nil, err
	}

	// Add common dial options
	opts = append(opts, f.dialOpts...)

	// Keep alive settings
	opts = append(opts, grpc.WithKeepaliveParams(keepalive.ClientParameters{
		Time:                10 * time.Second,
		Timeout:             60 * time.Second,
		PermitWithoutStream: true,
	}))

	// Use insecure mode if globally enforced, or if non-TLS port is specified
	if f.insecure || svc.Port != "443" {
		opts = append(opts, grpc.WithInsecure())
	} else {
		transportCreds, err := f.clientTransportCredentials(svc.Hostname)
		if err != nil {
			return nil, err
		}
		opts = append(opts, grpc.WithTransportCredentials(transportCreds))
	}

	return grpc.Dial(svc.Socket(), opts...)
}

func (f *Factory) clientTransportCredentials(serverName string) (credentials.TransportCredentials, error) {

	if f.rootCaPool == nil {
		p, err := x509.SystemCertPool()
		if err != nil {
			return nil, err
		}
		f.rootCaPool = p
	}

	conf := &tls.Config{
		ServerName: serverName,
		RootCAs:    f.rootCaPool,
	}

	return credentials.NewTLS(conf), nil
}
