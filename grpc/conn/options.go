package conn

import (
	"crypto/x509"

	"google.golang.org/grpc"
)

type FactoryOption func(*Factory)

func WithRootCaPool(p *x509.CertPool) FactoryOption {
	return func(f *Factory) {
		f.rootCaPool = p
	}
}

func WithDialOpts(opts []grpc.DialOption) FactoryOption {
	return func(f *Factory) {
		f.dialOpts = opts
	}
}

func WithInsecure() FactoryOption {
	return func(f *Factory) {
		f.insecure = true
	}
}
