package auth

import (
	"context"
	"fmt"
)

// IDToken immplements github.com/grpc/grpc-go/credentials.PerRPCCredentials. It loads the
// TokenSource from context and uses its id_token as a bearer.
type IDToken struct {
	Insecure bool
}

// RequireTransportSecurity immplements credentials.PerRPCCredentials.
func (i *IDToken) RequireTransportSecurity() bool {
	return !i.Insecure
}

//GetRequestMetadata immplements credentials.PerRPCCredentials.
func (i *IDToken) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {

	ts, err := TokenSourceFrom(ctx)
	if err != nil {
		return authorizationError(err)
	}

	tk, err := ts.Token()
	if err != nil {
		return authorizationError(err)
	}

	idtk, ok := tk.Extra("id_token").(string)
	if !ok {
		return authorizationError(fmt.Errorf("missing id_token"))
	}

	return authorizationHeader(tk.TokenType, idtk)
}
