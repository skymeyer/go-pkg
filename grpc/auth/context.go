package auth

import (
	"context"
	"errors"
	"net"

	oidc "github.com/coreos/go-oidc"
	"golang.org/x/oauth2"
)

type contextKey int

const (
	ctxTokenSource contextKey = iota
	ctxUserInfo
	ctxSourceIP
)

var (
	ErrTokenSource = errors.New("missing token source in context")
	ErrUserInfo    = errors.New("missing user info in context")
	ErrNoSourceIP  = errors.New("missing sourceip in context")
)

func TokenSourceFrom(ctx context.Context) (oauth2.TokenSource, error) {
	if ts, ok := ctx.Value(ctxTokenSource).(oauth2.TokenSource); ok {
		return ts, nil
	}
	return nil, ErrTokenSource
}

func WithTokenSource(ctx context.Context, ts oauth2.TokenSource) context.Context {
	return context.WithValue(ctx, ctxTokenSource, ts)
}

func UserInfoFrom(ctx context.Context) (*oidc.UserInfo, error) {
	if ui, ok := ctx.Value(ctxUserInfo).(*oidc.UserInfo); ok {
		return ui, nil
	}
	return nil, ErrUserInfo
}

func WithUserInfo(ctx context.Context, ui *oidc.UserInfo) context.Context {
	return context.WithValue(ctx, ctxUserInfo, ui)
}

func WithSourceIP(ctx context.Context, ip net.IP) context.Context {
	return context.WithValue(ctx, ctxSourceIP, ip)
}

func SourceIPFrom(ctx context.Context) (net.IP, error) {
	if ip, ok := ctx.Value(ctxSourceIP).(net.IP); ok {
		return ip, nil
	}
	return nil, ErrNoSourceIP
}
