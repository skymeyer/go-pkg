package auth

import (
	"context"
	"fmt"
	"net"
	"strings"

	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/peer"
)

// SourceIPAuth implements github.com/grpc-ecosystem/go-grpc-middleware/auth.AuthFunc
// and determines the source IP address of the caller in the following order:
//	- First entry in x-forwarded-for header if present
//	- Fallback to gRPC peer IP address
func SourceIPAuth(ctx context.Context) (context.Context, error) {

	var ipStr string

	// Get source IP from x-forwarded-for metadata header.
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if fwf, ok := md["x-forwarded-for"]; ok && len(fwf) == 1 {
			if list := strings.Split(fwf[0], ","); len(list) >= 1 {
				ipStr = list[0]
			}
		}
	}

	// Use gRPC peer object as a fallback.
	if ipStr == "" {
		if p, ok := peer.FromContext(ctx); ok {
			ipStr, _, _ = net.SplitHostPort(p.Addr.String())
		}
	}

	ip := net.ParseIP(ipStr)
	if ip == nil {
		return nil, fmt.Errorf("cannot determine source ip address")
	}

	return WithSourceIP(ctx, ip), nil
}
