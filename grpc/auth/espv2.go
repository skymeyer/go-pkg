package auth

import (
	"context"
	"encoding/base64"
	"encoding/json"

	oidc "github.com/coreos/go-oidc"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	ESPV2_USER_INFO_HEADER = "x-endpoint-api-userinfo"
)

// ESPv2Auth can be used by github.com/grpc-ecosystem/go-grpc-middleware/auth
// extracting the user info from gRPC metadata. No validation needs to be
// performed since ESPv2 already validated the token for us. Do not use this
// for deployments without a front-facing ESPv2 deployment.
type ESPv2Auth struct {
}

// AuthFunc implments github.com/grpc-ecosystem/go-grpc-middleware/auth.AuthFunc
func (a *ESPv2Auth) AuthFunc(ctx context.Context) (context.Context, error) {
	raw := metautils.ExtractIncoming(ctx).Get(ESPV2_USER_INFO_HEADER)
	if raw == "" {
		return nil, status.Errorf(codes.Unauthenticated, "userinfo missing")
	}

	data, err := base64.StdEncoding.DecodeString(raw)
	if err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "userinfo decode")
	}

	var ui = &oidc.UserInfo{}
	if err := json.Unmarshal(data, ui); err != nil {
		return nil, status.Errorf(codes.Unauthenticated, "userinfo format")
	}

	return WithUserInfo(ctx, ui), nil
}
