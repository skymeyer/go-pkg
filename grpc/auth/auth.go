package auth

import (
	"fmt"
)

func authorizationError(err error) (map[string]string, error) {
	return map[string]string{}, err
}

func authorizationHeader(kind, value string) (map[string]string, error) {
	return map[string]string{
		"authorization": fmt.Sprintf("%s %s", kind, value),
	}, nil
}
