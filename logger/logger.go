package logger

import (
	"fmt"
	"os"

	"github.com/blendle/zapdriver"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type environment string

var (
	envConsole environment = "console"
	envGCP     environment = "gcp"
)

func detectEnv() environment {
	if cloudRun := os.Getenv("K_SERVICE"); cloudRun != "" {
		return envGCP
	}
	if k8sSvc := os.Getenv("KUBERNETES_SERVICE_HOST"); k8sSvc != "" {
		return envGCP
	}
	return envConsole
}

func Initialize(level string) func() {
	logger, err := New(level)
	if err != nil {
		panic(fmt.Sprintf("global logger init: %v", err))
	}
	return zap.ReplaceGlobals(logger)
}

func New(level string) (*zap.Logger, error) {
	var config zap.Config

	// Detect environment
	env := detectEnv()
	if env == envGCP {
		config = zapdriver.NewProductionConfig()
	} else {
		config = zapdriver.NewDevelopmentConfig()
		config.Encoding = "console"
		config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	}

	// Set log level
	var l = zapcore.InfoLevel
	if err := l.UnmarshalText([]byte(level)); err != nil {
		return nil, err
	}
	config.Level = zap.NewAtomicLevelAt(l)

	// Create logger from configuration
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("zap.config.Build(): %v", err)
	}

	logger.Info("logger initialized",
		zap.String("environment", string(env)),
		zap.String("level", level),
	)

	return logger, nil
}
