package validation

import (
	"github.com/google/uuid"
	"k8s.io/apimachinery/pkg/util/validation"
)

func IsUUID(value string) []string {
	var errs []string
	if _, err := uuid.Parse(value); err != nil {
		errs = append(errs, err.Error())
	}
	return errs
}

func IsValidIP(value string) []string {
	return validation.IsValidIP(value)
}

func IsDNS1123Subdomain(value string) []string {
	return validation.IsDNS1123Subdomain(value)
}

func IsQualifiedName(value string) []string {
	return validation.IsQualifiedName(value)
}

func IsValidLabelValue(value string) []string {
	return validation.IsValidLabelValue(value)
}

func IsValidPortNum(port int) []string {
	return validation.IsValidPortNum(port)
}
