package osutil

import (
	"os"
	"path/filepath"
)

// EnsureConfigDir creates (if not exists) and returns a platform specific
// config directory for given application name.
func EnsureUserConfigDir(name string) (string, error) {
	return ensureUserDir(name, os.UserConfigDir)
}

// EnsureCacheDir creates (if not exists) and returns a platform specific
// config directory for given application name.
func EnsureUserCacheDir(name string) (string, error) {
	return ensureUserDir(name, os.UserCacheDir)
}

// EnsureUserDirs creates (if not exists) and returns platform specific
// config and cache directories for given application name.
func EnsureUserDirs(name string) (config, cache string, err error) {
	config, err = EnsureUserConfigDir(name)
	if err != nil {
		return "", "", err
	}
	cache, err = EnsureUserCacheDir(name)
	if err != nil {
		return "", "", err
	}
	return
}

type userDirFn func() (string, error)

func ensureUserDir(name string, userDir userDirFn) (string, error) {
	base, err := userDir()
	if err != nil {
		return "", err
	}
	dir := filepath.Join(base, name)
	if err := os.MkdirAll(dir, os.FileMode(0700)); err != nil {
		return "", err
	}
	return dir, nil
}
